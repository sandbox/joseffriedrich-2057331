Theme for http://kanzlei-deisser.de.

CSS id and class structure
==========================

 - #page
   - #header-columns
     - .region-header-firstcolumn
     - .region-header-secondcolumn
     - #secondary-menu
   - #header
     - #name-and-slogan
     - #theme-image
   - #navigation
     - #main-menu
     - .region-navigation
   - #main
     - #content
       - .region-highlighted
       - .region-content
     - #sidebars
       - .region-sidebar-first
       - .region-sidebar-second
   - #footer-columns
     - .region-footer-firstcolumn
     - .region-footer-secondcolumn
   - .region-footer
   - #tertiary-menu
 - .region-bottom
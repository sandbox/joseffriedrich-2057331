<?php

/**
 * @file
 * Theme setting callbacks for the "Kanzlei Deisser" theme.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function kanzlei_deisser_form_system_theme_settings_alter(&$form, &$form_state, $form_id = NULL)  {
  // Work-around for a core bug affecting admin themes. See issue #943212.

  if (isset($form_id)) {
    return;
  }

  $form['theme_settings']['toggle_tertiary_menu'] = array(
    '#type' => 'checkbox',
    '#title' => t('Tertiary menu'),
    '#default_value' => theme_get_setting('toggle_tertiary_menu'),
  );

  $form['tertiary_menu'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tertiary menu'),
  );

  $form['tertiary_menu']['tertiary_menu'] = array(
    '#type' => 'select',
    '#title' => t('Tertiary menu'),
    '#default_value' => theme_get_setting('tertiary_menu'),
    '#options' => menu_get_menus(),
  );

  $form['theme_image'] = array(
    '#type' => 'fieldset',
    '#title' => t('Theme Image'),
  );

  $form['theme_image']['kanzlei_deisser_image_fid'] = array(
    '#title' => t('Image'),
    '#type' => 'managed_file',
    '#description' => t('The uploaded image will be displayed on the theme.'),
    '#default_value' => theme_get_setting('kanzlei_deisser_image_fid'),
    '#upload_location' => 'public://theme_images/',
  );

  return $form;

}

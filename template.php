<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 */

/**
 * @mainpage
 *
 * @section project_page Project page
 * - @link http://drupal.org/sandbox/JosefFriedrich/2057331 Drupal sandbox project 2057331 @endlink
 *
 * @section git_repository Git repository
 * - @link http://drupalcode.org/sandbox/JosefFriedrich/2057331.git Drupal sandbox git repository 2057331 @endlink
 *
 * Theme for @link http://kanzlei-deisser.de kanzlei-deisser.de @endlink.
 *
 * @section id_and_css CSS id and class structure
 *
 * - #page
 *   - #header-columns
 *     - .region-header-firstcolumn
 *     - .region-header-secondcolumn
 *     - #secondary-menu
 *   - #header
 *     - #name-and-slogan
 *     - #theme-image
 *   - #navigation
 *     - #main-menu
 *     - .region-navigation
 *   - #main
 *     - #content
 *       - .region-highlighted
 *       - .region-content
 *     - #sidebars
 *       - .region-sidebar-first
 *       - .region-sidebar-second
 *   - #footer-columns
 *     - .region-footer-firstcolumn
 *     - .region-footer-secondcolumn
 *   - .region-footer
 *   - #tertiary-menu
 * - .region-bottom
 *
 * @section colors Colors
 *
 * - green: 9AC567
 * - green2: 7B945F
 * - green3: 548021
 * - green4: BCE291
 * - green5: C6E2A7
 * - green6: 629E1D
 *
 * - grey1: E5E5E5
 * - grey2: 8A9EA9
 * - grey3: 738B99
 * - grey4: 576A77
 * - grey5: 465660
 *
 * - red: BC6285
 * - beige: F5F3EB
 * - white: FFFFFF
 * - yellow: D9BC72
 * - blue: 82C4CE
 *
 * @section fonts Fonts
 *
 * - oxygen: Oxygen, Verdana, Tahoma, sans-serif
 * - courier:  Courier New, DejaVu Sans Mono, monospace, sans-serif
 *
 * @section files Files
 *
 * @subsection settings Settings
 *
 * - @link template.php template.php @endlink
 * - @link theme-settings.php theme-settings.php @endlink
 *
 * @subsection templates Templates
 *
 * - @link templates/page.tpl.php page.tpl.php @endlink
 */

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function kanzlei_deisser_preprocess_page(&$variables, $hook) {
  // Main menu
  if (theme_get_setting('toggle_main_menu')) {
    $main_menu_tree = menu_tree_all_data('main-menu');
    $variables['main_menu'] = menu_tree_output($main_menu_tree);
  }
  else {
    $variables['main_menu'] = NULL;
  }

  // Theme image
  $fid = theme_get_setting('kanzlei_deisser_image_fid');
  if ($file = file_load($fid)) {
    $path = $file->uri;
    $variables['theme_image'] = theme('image_style', array('style_name' => 'kanzlei_deisser', 'path' => $path));
  }
  else {
    $path = drupal_get_path('theme', 'kanzlei_deisser') . '/images/atrium.jpg';
    #$variables['theme_image'] = theme('image_style_path', array('style_name' => 'kanzlei_deisser', 'path' => $path));
    $variables['theme_image'] = theme('image', array('path' => $path));
  }

  // Tertiary menu
  if (theme_get_setting('toggle_tertiary_menu')) {
    $tertiary_links = theme_get_setting('tertiary_menu');
    $variables['tertiary_menu'] = menu_navigation_links($tertiary_links);
    $menus = function_exists('menu_get_menus') ? menu_get_menus() : menu_list_system_menus();
    $variables['tertiary_menu_heading'] = $menus[$tertiary_links];
  }
  else {
    $variables['tertiary_menu'] = '';
    $variables['tertiary_menu_heading'] = '';
  }
}

/**
 * Implements hook_image_styles_alter().
 */
function kanzlei_deissewr_image_styles_alter(&$styles) {
   $styles['kanzlei_deisser'] = array(
    'name' => 'kanzlei_deisser',
    'storage' => IMAGE_STORAGE_DEFAULT,
    'label' => 'kanzlei_deisser',
    'module' => 'Image',
    'effects' => array(
      array(
        'name' => 'image_scale_and_crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'label' => 'Scale and crop',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'Image',
        'data' => array(
          'width' => 850,
          'height' => 450,
          'upscale' => 1,
        ),
        'weight' => 0,
      ),
    ),
  );
}

/**
 * Implements hook_theme_registry_alter().
 */
function kanzlei_deisser_theme_registry_alter(&$theme_registry) {
  $theme_registry['image_style_path'] = array(
    'variables' => array(
      'style_name' => NULL,
      'path' => NULL,
      'alt' => '',
      'title' => NULL,
      'attributes' => array(),
   ),
   'function' => 'theme_image_style_path',
   'theme path' => drupal_get_path('theme', 'kanzlei_deisser'),
   'type' => 'theme',
  );
}

/**
 * This theme function is designed to deal with the limitation that
 * theme_image_style does not work with images outside the files directory.
 *
 * Usage is the same as theme_image_style.
 *
 * @param $variables
 */
function theme_image_style_path($variables) {
  $styled_path = image_style_path($variables['style_name'], $variables['path']);
  if (!file_exists($styled_path)) {
    $style = image_style_load($variables['style_name']);
    image_style_create_derivative($style, $variables['path'], $styled_path);
  }
  $variables['path'] = $styled_path;
  return theme('image', $variables);
}

/**
 * Add addtional informations as CSS classes to a menu item.
 *
 * Add the menu link ID (mlid) and depth count of the level as CSS classes to
 * all menu items.
 *
 * @see theme_menu_link()
 *
 * @ingroup themable
 */
function kanzlei_deisser_menu_link(array $variables) {
  $element = $variables['element'];

  $element['#attributes']['class'][] = 'menu-' . $element['#original_link']['mlid'];
  $element['#attributes']['class'][] = 'level-' . $element['#original_link']['depth'];

  $sub_menu = '';
  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Implements hook_css_alter().
 */
function kanzlei_deisser_css_alter(&$css) {
  unset($css['misc/ui/jquery.ui.core.css']);
  unset($css['misc/ui/jquery.ui.dialog.css']);
  unset($css['misc/ui/jquery.ui.resizable.css']);
  unset($css['misc/ui/jquery.ui.theme.css']);
  unset($css['modules/misc/ui/jquery.ui.button.css']);
  unset($css['modules/comment/comment.css']);
  unset($css['modules/field/theme/field.css']);
  unset($css['modules/node/node.css']);
  unset($css['modules/search/search.css']);
  unset($css['modules/system/system.menus.css']);
  unset($css['modules/system/system.messages.css']);
  unset($css['modules/user/user.css']);
  unset($css['sites/all/modules/print/css/printlinks.css']);
}
